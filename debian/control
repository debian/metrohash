Source: metrohash
Priority: optional
Maintainer: Alexander GQ Gerasiov <gq@debian.org>
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.6.0
Section: libs
Homepage: https://github.com/jandrewrogers/MetroHash
Vcs-Git: https://salsa.debian.org/debian/metrohash.git
Vcs-Browser: https://salsa.debian.org/debian/metrohash

Package: libmetrohash-dev
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, libmetrohash1 (= ${binary:Version})
Pre-Depends: ${misc:Pre-Depends}
Multi-Arch: same
Description: hash functions for non-cryptographic use cases (headers)
 Set of state-of-the-art hash functions for non-cryptographic use cases. They
 are notable for being algorithmically generated in addition to their
 exceptional performance. The set of published hash functions may be expanded
 in the future, having been selected from a very large set of hash functions
 that have been constructed this way.
 .
 This package provides library headers.

Package: libmetrohash1
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Multi-Arch: same
Description: hash functions for non-cryptographic use cases
 Set of state-of-the-art hash functions for non-cryptographic use cases. They
 are notable for being algorithmically generated in addition to their
 exceptional performance. The set of published hash functions may be expanded
 in the future, having been selected from a very large set of hash functions
 that have been constructed this way.
